
### Weather app with tags, now in react!

User can enter a name of the city in the search bar, upon doing so (with enter key), a new card will show up, displaying the weather conditions on returned location. It will also add a small button next to the search bar, and by clicking on it, user can remove that specific entry from display.

If another city is typed in, it will be added to the search bar, and to the display.

if multiple cities are entered, separated by comma, they will all be added to display, and as "tags" to the search bar.

Cities can also be removed by simply backspacing.

The app was developed in two versions: first one deviated in a way cities were entered: If cities were added one by one, display would only show that one entry, only way to show multiple results, was to type them together, separeated by comma.

Since instructions were unclear as to which approach seemed "right", I decided to keep cities adding up on enter, since it felt more intuitive.

Check out [live version](https://jolpazy.github.io/react-w/).

The app was made with [Create React App](https://github.com/facebookincubator/create-react-app).
