import { ADD, UPDATE, REMOVE, REMOVELAST } from "./actionTypes";

const addCity = city => {
  return {
    type: ADD,
    city
  };
};
const updateCity = (city, payload) => {
  return {
    type: UPDATE,
    city,
    payload
  };
};
export const removeCity = city => {
  return {
    type: REMOVE,
    city
  };
};
export const removeLast = () => {
  return { type: REMOVELAST };
};

export function submitThenUpdate(city) {
  return dispatch => {
    dispatch(addCity(city));
    return fetch(
      `https://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&APPID=171fb93bde138475dbc8d8d90938ac48`
    )
      .then(blob => blob.json())
      .then(data => {
        const payload = Object.assign({ fetching: false, city }, data);
        dispatch(updateCity(city, payload));
      });
  };
}
