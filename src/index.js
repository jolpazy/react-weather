import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import { cityReducer } from "./reducers";
import logger from "redux-logger";
import thunk from "redux-thunk";
import App from "./App";

const store = createStore(cityReducer, applyMiddleware(thunk, logger));

class AppWrapper extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <App />
      </Provider>
    );
  }
}

ReactDOM.render(<AppWrapper />, document.getElementById("root"));
