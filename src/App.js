import React from "react";
import { City } from "./City";
import { connect } from "react-redux";
import { mapStateToProps, mapDispatchToProps } from "./mapping";

export class App extends React.Component {
  handleInput = event => {
    if (event.key === "Enter") {
      let test = [...this.props.cities].map(item => {
        return item.call;
      });
      let arr = event.target.value.split(",");

      for (let i in arr) {
        let trimmed = arr[i].trim();
        if (!test.includes(trimmed) && trimmed !== "") {
          test.push(trimmed);
          this.props.submitThenUpdate(trimmed); // 1st dispatch to the store!!!
        }
      }
      event.target.value = "";
    }

    if (event.key === "Backspace" && event.target.value === "") {
      this.props.removeLast(); // dispatching to the store, removing last entry.
    }
  };

  remove = id => () => this.props.removeCity(id); //dispatching to the store, filtering.

  render = () => {
    const children = this.props.cities.map(item => {
      return <City city={item} key={item.call} />;
    });

    const buttons = this.props.cities.map(item => {
      return (
        <button
          className="btn"
          onClick={this.remove(item.call)}
          key={item.call}
        >
          {item.call}{" "}
          <span className="x">
            <sup>x</sup>
          </span>
        </button>
      );
    });

    return (
      <div>
        <div className="nav">
          <div className="bar">
            {buttons}
            <input
              placeholder="Search for a city, or a few (:"
              onKeyDown={this.handleInput}
            />
          </div>
        </div>
        <div className="display">{children}</div>
      </div>
    );
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
