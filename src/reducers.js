import { ADD, UPDATE, REMOVE, REMOVELAST } from "./actionTypes";

export const cityReducer = (state = [], action) => {
  switch (action.type) {
    case ADD: {
      return state.concat(
        Object.assign({
          call: action.city,
          fetching: true
        })
      );
    }
    case UPDATE: {
      return state.map(item => {
        if (item.call === action.payload.city) {
          return Object.assign(item, action.payload);
        } else return item;
      });
    }
    case REMOVE:
      return state.filter(item => item.call !== action.city);
    case REMOVELAST:
      return state.slice(0, state.length - 1);
    default:
      return state;
  }
};
