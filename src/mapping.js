import { removeCity, removeLast, submitThenUpdate } from "./actions";

export const mapStateToProps = state => {
  return { cities: state };
};

export const mapDispatchToProps = dispatch => {
  return {
    removeCity: city => {
      dispatch(removeCity(city));
    },
    removeLast: () => {
      dispatch(removeLast());
    },
    submitThenUpdate: city => {
      dispatch(submitThenUpdate(city));
    }
  };
};
