export const timeConverter = timestamp => {
  const a = new Date(timestamp * 1000);
  const months = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
  ];

  const month = months[a.getMonth()];
  const date = a.getDate();
  const time = `${date} ${month}`;

  return time;
};
