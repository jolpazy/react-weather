import React from "react";
import { timeConverter } from "./helpers";
import "./index.css";

export const City = props => {
  //probabaly not the best way to handle error:
  if (parseInt(props.city.cod, 10) >= 400) {
    const call = props.city.call;
    return (
      <div className="city">
        <div className="left" />
        <div className="mid">
          {call} not found ):
          <br />
        </div>
        <div className="right" />
      </div>
    );
  }
  if (!props.city.fetching) {
    const { dt, name } = props.city;
    const temp = props.city.main.temp;
    const wind = props.city.wind.speed;
    const description = props.city.weather[0].description;
    const humidity = props.city.main.humidity;

    return (
      <div className="city">
        <div className="left">
          <strong>{name}</strong>
          <br />
          {timeConverter(dt)}
        </div>
        <div className="mid">
          <span className="temp">
            <strong>
              {temp}˚<sup>c</sup>
            </strong>
          </span>
          <br />
          {description}
        </div>
        <div className="right">
          Wind: {wind} km/s<br />Humidity: {humidity}% <br />
        </div>
      </div>
    );
  } else {
    const call = props.city.call;
    return (
      <div className="city">
        <div className="left" />
        <div className="mid">
          Searching for {call}...
          <br />
        </div>
        <div className="right" />
      </div>
    );
  }
};
